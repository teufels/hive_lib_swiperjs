![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__lib__swiperjs-blue.svg)
![version](https://img.shields.io/badge/version-6.5.9-yellow.svg?style=flat-square)

Swiper
==========

Swiper - is the free and most modern mobile touch slider with hardware accelerated transitions and amazing native behavior. It is intended to be used in mobile websites, mobile web apps, and mobile native/hybrid apps.

Swiper is not compatible with all platforms, it is a modern touch slider which is focused only on modern apps/platforms to bring the best experience and simplicity.

#### This version supports TYPO3

![CUSTOMER](https://img.shields.io/badge/9_LTS-%23A3C49B.svg?style=flat-square)
![CUSTOMER](https://img.shields.io/badge/10_LTS-%23A3C49B.svg?style=flat-square)

#### Composer support
`composer req beewilly/hive_lib_swiperjs`

## Source
  * [Bundles](https://unpkg.com/browse/swiper@6.5.9/)
  * [github](https://github.com/nolimits4web/swiper/tree/master/src)

## Getting Started
  * [Getting Started Guide](https://swiperjs.com/get-started/)
  * [API](https://swiperjs.com/api/)
  * [Demos](https://swiperjs.com/demos/)
  
## NOTES
- Extension Version corresponds swiper.js Version
- Extension includes nessasary JS/CSS