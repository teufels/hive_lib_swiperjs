<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_lib_swiperjs', 'Configuration/TypoScript', 'hive_lib_swiperjs');

    }
);
